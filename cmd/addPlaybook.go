// Copyright © 2017 Julien Garcia Gonzalez <garciagonzalez.julien@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"gitlab.com/jgsqware/playbook-cli/master"
	"gitlab.com/jgsqware/playbook-cli/master/playbook"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// addPlaybookCmd represents the addPlaybook command
var addPlaybookCmd = &cobra.Command{
	Use:   "playbook",
	Short: "Add a playbook to a master file",
	Long:  `Add a playbook to a master file`,
	Run: func(cmd *cobra.Command, args []string) {
		m, err := master.Load(viper.GetString("master"))

		if err != nil {
			fmt.Println("master cannot be loaded: ", err)
			os.Exit(1)
		}

		p, err := playbook.Load(args[0])

		if err != nil {
			fmt.Println("playbook cannot be loaded or doesn't exist: ", err)
			os.Exit(1)
		}
		m.IncludePlaybook(p.Name)
		err = m.Write()
		if err != nil {
			fmt.Println("error writing playbook: ", err)
			os.Exit(1)
		}
	},
}

func init() {
	addCmd.AddCommand(addPlaybookCmd)
	addPlaybookCmd.Flags().StringP("master", "m", defaultMasterName, "parent master")
	viper.BindPFlag("master", addPlaybookCmd.Flags().Lookup("master"))

}
