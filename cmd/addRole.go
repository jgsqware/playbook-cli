// Copyright © 2017 Julien Garcia Gonzalez <garciagonzalez.julien@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/jgsqware/playbook-cli/master/playbook"
	"gitlab.com/jgsqware/playbook-cli/master/role"
)

// addRoleCmd represents the addRole command
var addRoleCmd = &cobra.Command{
	Use:   "role",
	Short: "Add a role to a playbook",
	Long:  `Add a role to a playbook`,
	Run: func(cmd *cobra.Command, args []string) {
		p, err := playbook.Load(viper.GetString("playbook"))

		if err != nil {
			fmt.Println("playbook cannot be loaded: ", err)
			os.Exit(1)
		}

		r, err := role.Load(args[0])

		if err != nil {
			fmt.Println("role cannot be loaded or doesn't exist: ", err)
			os.Exit(1)
		}
		p.AddRole(r.Name)
		err = p.Write()
		if err != nil {
			fmt.Println("error writing playbook: ", err)
			os.Exit(1)
		}
	},
}

func init() {
	addCmd.AddCommand(addRoleCmd)
	addRoleCmd.Flags().StringP("playbook", "p", defaultPlaybookName, "parent playbook")
	viper.BindPFlag("playbook", addRoleCmd.Flags().Lookup("playbook"))

}
