package playbook

import (
	"io/ioutil"
	"os"

	"gitlab.com/jgsqware/playbook-cli/utils"

	"github.com/jgsqware/yaml"
)

type Playbook struct {
	Name  string
	Roles []playbook `yaml: ",omitEmpty`
}

type playbook struct {
	Hosts          []string
	AnyErrorsFatal bool `yaml:"any_errors_fatal"`
	Roles          []string
}

func Init(name string) Playbook {
	p, err := Load(name)
	if err != nil {
		name = utils.AppendYMLExt(name)
		p = Playbook{Name: name}

		p.Roles = append(p.Roles, playbook{
			AnyErrorsFatal: true,
		})
	}
	return p
}

func Load(name string) (Playbook, error) {
	name = utils.AppendYMLExt(name)
	if _, err := os.Stat(name); err != nil {
		return Playbook{}, err
	}

	p := Playbook{Name: name}
	data, err := ioutil.ReadFile(name)
	if err != nil {
		return Playbook{}, err
	}

	err = yaml.NewStrictDecoder().Unmarshal([]byte(data), &(p.Roles))
	if err != nil {
		return Playbook{}, err
	}

	return p, nil
}

func (p *Playbook) AddRole(r string) {

	pr := &p.Roles[0]
	for _, p := range pr.Roles {
		if p == r {
			return
		}
	}
	pr.Roles = append(pr.Roles, r)
}

func (p Playbook) ToYAML() ([]byte, error) {
	d, err := yaml.Marshal(&(p.Roles))
	if err != nil {
		return nil, err
	}
	return d, nil
}

func (p *Playbook) Write() error {
	var err error
	c := []byte{}
	if len(p.Roles) != 0 {
		c, err = p.ToYAML()
		if err != nil {
			return err
		}
	}
	err = ioutil.WriteFile(p.Name, []byte(c), 0644)
	if err != nil {
		return err
	}

	return nil
}
