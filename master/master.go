package master

import (
	"io/ioutil"
	"os"

	"gitlab.com/jgsqware/playbook-cli/master/inventory"
	"gitlab.com/jgsqware/playbook-cli/master/role"
	"gitlab.com/jgsqware/playbook-cli/utils"

	yaml "github.com/jgsqware/yaml"
)

type Master struct {
	Name     string
	Includes []include
}

type include struct {
	Include string `yaml: "include,omitempty,flow"`
}

func (m *Master) IncludePlaybook(playbook string) {
	for _, p := range m.Includes {
		if p.Include == playbook {
			return
		}
	}

	m.Includes = append(m.Includes, include{Include: playbook})
}

func (m Master) ToYAML() ([]byte, error) {
	d, err := yaml.Marshal(&(m.Includes))
	if err != nil {
		return nil, err
	}
	return d, nil
}

func (m *Master) Write() error {
	var err error
	c := []byte{}
	if len(m.Includes) != 0 {
		c, err = m.ToYAML()
		if err != nil {
			return err
		}
	}
	err = ioutil.WriteFile(m.Name, []byte(c), 0644)
	if err != nil {
		return err
	}

	return nil
}

func initStructure() error {
	err := role.InitStructure()
	if err != nil {
		return err
	}
	err = inventory.InitStructure()
	if err != nil {
		return err
	}
	return nil
}

func Init(name string) Master {
	initStructure()
	m, err := Load(name)
	if err != nil {
		name = utils.AppendYMLExt(name)
		return Master{Name: name}
	}
	return m
}

func Load(name string) (Master, error) {
	name = utils.AppendYMLExt(name)
	if _, err := os.Stat(name); err != nil {
		return Master{}, err
	}

	m := Master{Name: name}
	data, err := ioutil.ReadFile(name)
	if err != nil {
		return Master{}, err
	}

	err = yaml.NewStrictDecoder().Unmarshal([]byte(data), &(m.Includes))
	if err != nil {
		return Master{}, err
	}

	return m, nil
}
