package inventory

import (
	"os"
	"path/filepath"
)

var (
	Path             = filepath.Join(".", "inventories")
	defaultInventory = []string{"production", "staging"}
	inventoryFolders = []string{"group_vars", "host_vars"}
)

type Inventory struct {
	Name string
}

func InitStructure() error {
	for _, f := range defaultInventory {
		initInventoryStructure(f)
	}
	return nil
}

func initInventoryStructure(name string) error {
	current := filepath.Join(Path, name)
	if err := os.MkdirAll(current, os.ModePerm); err != nil {
		return err
	}
	for _, i := range inventoryFolders {
		if err := os.MkdirAll(filepath.Join(current, i), os.ModePerm); err != nil {
			return err
		}
	}
	if _, err := os.OpenFile(filepath.Join(current, "hosts"), os.O_RDONLY|os.O_CREATE, 0666); err != nil {
		return err
	}
	return nil
}

func Init(name string) Inventory {
	initInventoryStructure(name)

	return Inventory{
		Name: name,
	}
}
