package role

import (
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

var (
	Path    = filepath.Join(".", "roles")
	folders = []string{"tasks", "handlers", "templates", "files", "vars", "defaults", "meta"}
)

type Role struct {
	Name  string
	Tasks []string
}

func fPath(name, f string) string {
	return filepath.Join(Path, name, f)
}

func InitStructure() error {
	return os.MkdirAll(Path, os.ModePerm)
}

func initRoleStructure(name string) error {
	for _, f := range folders {
		fp := fPath(name, f)
		if err := os.MkdirAll(fp, os.ModePerm); err != nil {
			return err
		}
		if _, err := os.OpenFile(filepath.Join(fp, "main.yml"), os.O_RDONLY|os.O_CREATE, 0666); err != nil {
			return err
		}

	}
	return nil
}

func Init(name string) Role {
	initRoleStructure(name)
	r, err := Load(name)
	if err != nil {
		r := Role{Name: name}
		r.loadTasks()
	}
	return r
}

func (r *Role) loadTasks() {
	files, _ := ioutil.ReadDir(fPath(r.Name, "tasks"))
	for _, f := range files {
		r.Tasks = append(r.Tasks, strings.TrimSuffix(f.Name(), path.Ext(f.Name())))
	}
}

func Load(name string) (Role, error) {
	if _, err := os.Stat(fPath(name, "")); err != nil {
		return Role{}, err
	}
	r := Role{Name: name}
	r.loadTasks()
	return r, nil
}
