package vagrant

import (
	"os"
	"text/template"
)

func Generate(box, name string) error {
	data, err := Asset("template/Vagrantfile")
	if err != nil {
		return err
	}
	t, err := template.New("vangrantfile").Parse(string(data))
	if err != nil {
		return err
	}
	f, err := os.OpenFile("Vagrantfile", os.O_CREATE|os.O_WRONLY, 0655)
	if err != nil {
		return err
	}
	defer f.Close()

	err = t.Execute(f,
		struct {
			Box, Name string
		}{
			box,
			name,
		})

	if err != nil {
		return err
	}
	return nil
}
