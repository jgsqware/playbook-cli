package utils

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func AppendYMLExt(name string) string {
	if filepath.Ext(name) == "" {
		name += ".yml"
	}
	return name
}

func Ask(request, defaultValue string) (string, error) {
	reader := bufio.NewReader(os.Stdin)
	fmt.Printf("%v [%v]:", request, defaultValue)

	response, err := reader.ReadString('\n')
	if err != nil {
		return "", err
	}

	response = strings.ToLower(strings.TrimSpace(response))
	if response == "" {
		response = defaultValue
	}
	return response, nil
}

func AskForConfirmation(s string) bool {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Printf("%s [y/n]: ", s)

		response, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		response = strings.ToLower(strings.TrimSpace(response))

		if response == "y" || response == "yes" {
			return true
		} else if response == "n" || response == "no" {
			return false
		}
	}
}
