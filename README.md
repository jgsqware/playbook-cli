[![build status](https://gitlab.com/jgsqware/playbook-cli/badges/master/build.svg)](https://gitlab.com/jgsqware/playbook-cli/commits/master)

playbook-cli
-----------------
> A Commander for your ansible playbook

# Installation

    go get gitlab.com/jgsqware/playbook-cli

## Usage

### Init the project

```bash
playbook-cli init my-project
```

### Create a playbook

```bash
playbook-cli create playbook my-playbook
```

### Create a role

```bash
playbook-cli create role my-role
```

### Create an inventory

```bash
playbook-cli create inventory my-inventory
```

### Create an Vagrantfile

```bash
playbook-cli create -m my-project -b debian:jessie vagrantfile
```

### Add a playbook to the master file

```bash
playbook-cli add playbook -m my-project my-playbook
```

### Add a role to a playbook

```bash
playbook-cli add role -p my-playbook my-role
```